# UI Browser-based Client

UI stands for 'User Interface'.

A front-end (web-based) application.

The part of the application that gets delivered to the client / browser for the person using the service.

Javascirpt (JS) based because Javascript is the language of the web.

## Working with node under Docker

Configurations live in `docker-compose.yml`

Set the UI container to the correct run mode. For debugging and setting up, make sure the container stays running with:

    entrypoint: ["tail", "-f", "/dev/null"]

If the container is crashing, it may be necessary to set it to always on / debug mode

Assumes the containers are running from here on [See main container notes for more details](../README-docker.md)

Connect to the containers

    docker-compose exec ui bash

Once you have a shell on the container, you can troubleshoot as necessary.

    yarn

Try running the UI build step interactively. Running interactively is useful during development and debugging cycles.

## Vite

https://vitejs.dev/guide/#overview

yarn create vite boilerplate --template vue

mv boilerplate/\* .

mv boilerplate/.gitignore .
rmdir boilerplate/

yarn

edit package.json:

```
"scripts": {
  "dev": "vite --host 0.0.0.0",
}
```

yarn dev

be sure to update the host name. That's the important bit from before.

### Disabled automated start of browser by vite

Sometimes it's passed as the `--open` parameter in `package.json`

Other times it's in the `vite.config.js` file:

```ui/vite.config.js
  server: {
    // open: true,
  },
```

It will result in an error because the container doesn't have a browser:

```
events.js:292
      throw er; // Unhandled 'error' event
      ^

Error: spawn xdg-open ENOENT
    at Process.ChildProcess._handle.onexit (internal/child_process.js:269:19)
    at onErrorNT (internal/child_process.js:465:16)
    at processTicksAndRejections (internal/process/task_queues.js:80:21)
Emitted 'error' event on ChildProcess instance at:
    at Process.ChildProcess._handle.onexit (internal/child_process.js:275:12)
    at onErrorNT (internal/child_process.js:465:16)
    at processTicksAndRejections (internal/process/task_queues.js:80:21) {
  errno: -2,
  code: 'ENOENT',
  syscall: 'spawn xdg-open',
  path: 'xdg-open',
  spawnargs: [ 'http://localhost:3000/' ]
}
error Command failed with exit code 1.
```

## Browser

Open a browser and see if it works! :)

Or run the tests via Cypress. Either way!
