# vite-vue3-tailwind-starter Project

https://github.com/web2033/vite-vue3-tailwind-starter

```sh
cd ui
npx degit web2033/vite-vue3-tailwind-starter vvt-app

mv vvt-app/public/* public/
rmdir vvt-app/public

mv vvt-app/src/components/* src/components/
rmdir vvt-app/src/components

mv vvt-app/src/* src/
rmdir vvt-app/src

mv vvt-app/* .
mv vvt-app/.git* .

mv README.md README-vvt.md
rm package-lock.json

git add .
git commit -m "merge in degit version of vvt"

```

b/ui/package.json

"name": "boilerplate",

- "dev": "vite --host 0.0.0.0",

git add .
git commit -m "update package name and enable localhost"

/ui/vite.config.js

- server: {
- open: true,
- },

git commit -m "don't try to open browser automatically"

configure the containers, servers, and make sure tests run.

docker-compose exec ui bash

rm -r node_modules/\*

git remote add origin git@gitlab.com:fern-seed/web-vvt-api-data.git
git push -u --force origin main

# should fail -- change settings on repo if you have that level of access

otherwise, not a desired operation in most cases
