# Fern Seed

a pattern for structuring web apps  
a minimalist and flexible container boilerplate  
a repeatable way to organize projects  
a foundation to configure a full-stack web-app  
a meta framework to use as an application architecture template  
a blank slate, a fresh start  
focused on devx with a clear path toward production

https://opensource.guide/starting-a-project/

## Containers

This pattern leverages docker containers and docker-compose. If you're new to containers or need to get docker installed, [see here](https://gitlab.com/charlesbrandt/public/-/blob/main/system/virtualization/docker.md).

## Setup

```
git clone -b main --single-branch git@gitlab.com:fern-seed/web-ui-api-data.git boilerplate
cd boilerplate
git remote -v
git remote rename origin upstream
git remote set-url --push upstream no_push
git remote -v
```

Set up the path to your own project's git server (e.g. gitlab, github or a local git server):

```
git remote add origin git@gitlab.com:fern-seed/boilerplate.git
git push -u origin main
# or just
git push -u origin
```

## Existing projects

Already have an existing project that would benefit from an improved structure? Add in web-ui-api-data to the upstream

```
git remote -v

git remote add upstream https://gitlab.com/fern-seed/web-ui-api-data

git fetch upstream
git merge --allow-unrelated-histories upstream/main
# or whichever branch you want to merge
```

## Pull in Upstream Updates

To track changes on a remote upstream web-ui-api-data and merge them in with your repo:

```
git remote -v

git remote add upstream https://gitlab.com/fern-seed/web-ui-api-data

git fetch upstream
git merge upstream/main
```

or just ?

```
git pull upstream main
```

## Conventions / Patterns

use the word 'boilerplate' for project level naming
dashes usually ok, low lines usually ok, _no spaces_, ideally no capitalization

use the string 'web-ui-api-data' for printable name
anything goes here, including spaces and capitalization, as long as it's a permanent string. This string should not change over time. If it does change in one place it should be changed everywhere.

https://boilerplate.local
http://boilerplate.local
boilerplate.local
should be used for the public address of the application

https://gitlab.com/fern-seed/web-ui-api-data/-/issues
the project's issues URL

https://gitlab.com/fern-seed/web-ui-api-data
the project's repository URL

TODO:admins@web-ui-api-data.email
Point of contact:

charlesbrandt
Copyright and username references

Move from more specific to less specific to match all cases

### Find / Replace

Find and replace all instances of `boilerplate` with `project_name`. Use VS Code to find and replace across all files and see what is being matched. Be careful if any of the strings appear in your .git directory.

git add .
git commit -m "update project name and references"

## Next Steps

[Infrastructure configuration (Dev-Ops)](README-docker.md)

    git mv README.md README-web-ui-api-data.md

If you want to keep it around as a reference. (It's already in your git repo, so it's not costing you space. ;) )

Or if you want to keep things clean

    git rm README.md

This breaks the association (a good thing!) so you won't keep getting conflicts in your README.md files when trying to merge upstream.

    cd ui
    yarn
    yarn run cypress open

## Additional documentation

    data/\*
    ui/content/\*

## Using git repos as templates

[Having a git repo that is a template for new projects](https://medium.com/@smrgrace/having-a-git-repo-that-is-a-template-for-new-projects-148079b7f178) by Sam Grace is a well written overview of the process.

https://betterprogramming.pub/forget-boilerplate-use-repository-templates-74efebbee8eb
